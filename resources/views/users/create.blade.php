@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h1 class="text-center"> Crear Usuarios</h1>
        </div>
        <div class="card-body">
            <form action="{{route('users.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Escribe tu nombre" required>
                </div>
                <div class="form-group">
                    <label for="email">Correo Electronico</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Escribe tu correo" required>
                </div>
                <div class="form-group">
                    <label for="password">Contraseña</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="*********" required>
                </div>
                <div class="form-group">
                    <label for="role">Rol</label>
                    <select class="form-control" id="role" name="role" required>
                       @foreach($roles as $role)
                            <option value="{{$role->id}}">{{$role->value}}</option>
                       @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-info" type="submit">Agregar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
