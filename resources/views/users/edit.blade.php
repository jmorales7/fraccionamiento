@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h1 class="text-center"> Editar Usuarios</h1>
        </div>
        <div class="card-body">
            <form action="{{route('users.update',$user->id)}}" method="post">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="name">Nombre</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Escribe tu nombre" value="{{$user->name}}" required>
                </div>
                <div class="form-group">
                    <label for="email">Correo Electronico</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Escribe tu correo" value="{{$user->email}}" required>
                </div>
                <div class="form-group">
                    <label for="password">Contraseña</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="*********" required>
                </div>
                <div class="form-group">
                    <label for="role">Rol</label>
                    <select class="form-control" id="role" name="role" required>
                        @foreach($roles as $role)
                            <option value="{{$role->id}}"
                            @if($role->id == $user->role_id)
                                selected
                            @endif
                            >{{$role->value}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-info" type="submit">Editar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
