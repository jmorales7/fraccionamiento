@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <table id="payment-table">
            <thead>
            <tr>
                <th scope="col">Concepto</th>
                <th scope="col">Cantidad</th>
                <th scope="col">Estatus</th>
                <th scope="col">Usuario</th>
                <th scope="col">Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($payments as $payment )
                <tr>
                    <th scope="row">{{ $payment->concept }}</th>
                    <th scope="row">{{ $payment->quantity }}</th>
                    <th scope="row">{{ $payment->status == 'C' ? 'Completado' : 'No Completado' }}</th>
                    <th scope="row">{{ $payment->user ? $payment->user->name :'N/A' }}</th>
                    <td>
                        @if (Auth::user()->role_id == 1)
                            <a href="{{route('maintenance-payments.edit',$payment->id)}}" class="btn btn-warning btn-sm">Editar</a>
                            <button data-toggle="modal" data-target="#exampleModal" data-id="{{$payment->id}}" class="btn btn-danger btn-sm eliminar">Eliminar</button>
                        @else
                            <p>N/A</p>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Eliminar Pago</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5>Seguro que deseas eliminar</h5>
                    <form id="form-delete" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">Si</button>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            $(document).ready( function () {
                $('#payment-table').DataTable();

                $('.eliminar').click(function () {
                    let id = $(this).data('id')
                    $('#form-delete').attr('action', '/maintenance-payments/'+ id);
                })
            } );
        </script>
    @endpush

@endsection
