@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h1 class="text-center"> Editar Pago</h1>
        </div>
        <div class="card-body">
            <form action="{{route('maintenance-payments.update',$payment->id)}}" method="post">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="concept">Concepto</label>
                    <input type="text" class="form-control" id="concept" name="concept" placeholder="Escribe el valor" value="{{$payment->concept}}" readonly>
                </div>
                <div class="form-group">
                    <label for="quantity">Cantidad</label>
                    <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Escribe el valor" value="{{$payment->quantity}}" readonly>
                </div>
                <div class="form-group">
                    <label for="status">Estatus</label>
                    <select name="status" id="status">
                        <option value="C">Completado</option>
                        <option value="O">No Completado</option>
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-info" type="submit">Editar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
