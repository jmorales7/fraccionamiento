@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h1 class="text-center"> Crear Pago</h1>
        </div>
        <div class="card-body">
            <form action="{{route('maintenance-payments.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="concept">Concepto</label>
                    <input type="text" class="form-control" id="concept" name="concept" placeholder="Escribe el valor"  required>
                </div>
                <div class="form-group">
                    <label for="quantity">Cantidad</label>
                    <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Escribe el valor" required>
                </div>
                <div class="form-group">
                    <label for="status">Estatus</label>
                    <select name="status" id="status" class="form-control">
                        <option value="C">Completado</option>
                        <option value="O">No Completado</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="status">Usuario</label>
                    <select name="user" id="status" class="form-control">
                        @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-info" type="submit">Agregar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
