@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h1 class="text-center"> Crear Pago</h1>
        </div>
        <div class="card-body">
            <form action="{{route('payments.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="concept">Concepto</label>
                    <input type="text" class="form-control" id="concept" name="concept" placeholder="Escribe el valor"  required>
                </div>
                <div class="form-group">
                    <label for="quantity">Cantidad</label>
                    <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Escribe el valor" required>
                </div>
                <div class="form-group">
                    <label for="status">Usuario</label>
                    <select name="user" id="status" class="form-control">
                        @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="file">archivo</label>
                    <input type="file" class="form-control" id="file" name="image" required>
                </div>
                <div class="form-group">
                    <button class="btn btn-info" type="submit">Agregar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
