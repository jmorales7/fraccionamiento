@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h1 class="text-center"> Crear Pago</h1>
        </div>
        <div class="card-body">
            <form action="{{route('payments.update',$payment->id)}}" method="post">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="concept">Concepto</label>
                    <input type="text" class="form-control" id="concept" name="concept" placeholder="Escribe el valor" value="{{$payment->concept}}"  required>
                </div>
                <div class="form-group">
                    <label for="quantity">Cantidad</label>
                    <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Escribe el valor" value="{{$payment->quantity}}" required>
                </div>
                <div class="form-group">
                    <label for="status">Usuario</label>
                    <select name="user" id="status" class="form-control">
                        @foreach($users as $user)
                            <option
                                @if ($user->id === $payment->user_id)
                                    selected
                                @endif
                                value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-info" type="submit">Editar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
