@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h1 class="text-center"> Ver Pago</h1>
        </div>
        <div class="card-body">
            <form action="{{route('payments.update',$payment->id)}}" method="post">
                <div class="form-group">
                    <label for="concept">Concepto</label>
                    <input type="text" class="form-control" id="concept" name="concept" placeholder="Escribe el valor" value="{{$payment->concept}}"  readonly>
                </div>
                <div class="form-group">
                    <label for="quantity">Cantidad</label>
                    <input type="number" class="form-control" id="quantity" name="quantity" placeholder="Escribe el valor" value="{{$payment->quantity}}" readonly>
                </div>
                <div class="form-group">
                    <label for="status">Usuario</label>
                    <select name="user" id="status" class="form-control" readonly>
                        @foreach($users as $user)
                            <option
                                @if ($user->id === $payment->user_id)
                                selected
                                @endif
                                value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                </div>
            </form>
            @foreach($files as $file)
                <img src="{{env('APP_URL').\Illuminate\Support\Facades\Storage::url($file->file_url)}}" class="card-img-top" style="width: 150px" alt="">
            @endforeach
        </div>
    </div>
@endsection
