@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h1 class="text-center"> Editar Reservación</h1>
        </div>
        <div class="card-body">
            <form action="{{route('schedules.update',$schedule->id)}}" method="post">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="date">Fecha</label>
                    <input type="date" class="form-control" id="date" name="date" value="{{$schedule->date}}" readonly>
                </div>
                <div class="form-group">
                    <label for="description">Descripcón</label>
                    <textarea name="description" id="description" cols="30" rows="10" readonly>{{$schedule->description}}</textarea>
                </div>
                <div class="form-group">
                    <label for="status">Estatus</label>
                    <select name="status" id="status">
                        <option value="A">Aprobado</option>
                        <option value="D">Declinado</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="status">Usuario</label>
                    <select name="user" id="status" class="form-control">
                        @foreach($users as $user)
                            <option
                                @if ($user->id === $schedule->user_id)
                                selected
                                @endif
                                value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-info" type="submit">Editar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
