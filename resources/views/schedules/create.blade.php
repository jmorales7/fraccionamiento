@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h1 class="text-center"> Crear Reservación</h1>
        </div>
        <div class="card-body">
            <form action="{{route('schedules.store')}}" method="post">
                @csrf
                @if (session('status'))
                    <div class="alert alert-warning">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="form-group">
                    <label for="date">Fecha</label>
                    <input type="date" class="form-control" id="date" name="date" required>
                </div>
                <div class="form-group">
                    <label for="description">Descripcón</label>
                    <textarea name="description" id="description" cols="30" rows="10" required></textarea>
                </div>
                <div class="form-group">
                    <label for="status">Estatus</label>
                    <select name="status" id="status">
                        <option value="A">Aprobado</option>
                        <option value="D">Declinado</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="status">Usuario</label>
                    <select name="user" id="status" class="form-control">
                        @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-info" type="submit">Agregar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
