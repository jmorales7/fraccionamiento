<nav id="sidebar">
    <div class="sidebar-header">
        <h3>Fraccionamiento</h3>
    </div>
    @auth
        <ul class="list-unstyled components">
            <p class="text-center">{{ Auth::user()->name }}</p>
            @if (Auth::user()->role_id == 1)
                <li class="active">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Usuarios</a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="{{route('users.index')}}">Lista</a>
                        </li>
                        <li>
                            <a href="{{route('users.create')}}">Crear</a>
                        </li>
                    </ul>
                </li>
            @endif


            <li class="active">
                <a href="#pay-m" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Pagos Mantenimiento</a>
                <ul class="collapse list-unstyled" id="pay-m">
                    <li>
                        <a href="{{route('maintenance-payments.index')}}">Lista</a>
                    </li>
                    <li>
                        <a href="{{route('maintenance-payments.create')}}">Crear</a>
                    </li>
                </ul>
            </li>
            <li class="active">
                <a href="#pay" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Pagos</a>
                <ul class="collapse list-unstyled" id="pay">
                    <li>
                        <a href="{{route('payments.index')}}">Lista</a>
                    </li>
                    @if (Auth::user()->role_id == 1)
                        <li>
                            <a href="{{route('payments.create')}}">Crear</a>
                        </li>
                    @endif

                </ul>
            </li>
            <li class="active">
                <a href="#schedule" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Reservaciones</a>
                <ul class="collapse list-unstyled" id="schedule">
                    <li>
                        <a href="{{route('schedules.index')}}">Lista</a>
                    </li>
                    <li>
                        <a href="{{route('schedules.create')}}">Crear</a>
                    </li>
                </ul>
            </li>
        </ul>
    @endauth

</nav>
