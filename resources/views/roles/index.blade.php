@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <table id="role-table">
            <thead>
            <tr>
                <th scope="col">Value</th>
                <th scope="col">Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($roles as $role )
                <tr>
                    <th scope="row">{{ $role->value }}</th>
                    <td>
                        <a href="{{route('roles.edit',$role->id)}}" class="btn btn-warning btn-sm">Editar</a>
                        <button data-toggle="modal" data-target="#exampleModal" data-id="{{$role->id}}" class="btn btn-danger btn-sm eliminar">Eliminar</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Eliminar Rol</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h5>Seguro que deseas eliminar</h5>
                    <form id="form-delete" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">Si</button>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            $(document).ready( function () {
                $('#role-table').DataTable();

                $('.eliminar').click(function () {
                    let id = $(this).data('id')
                    $('#form-delete').attr('action', '/roles/'+ id);
                })
            } );
        </script>
    @endpush

@endsection
