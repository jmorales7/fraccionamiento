@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h1 class="text-center"> Crear Rol</h1>
        </div>
        <div class="card-body">
            <form action="{{route('roles.store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="value">Valor</label>
                    <input type="text" class="form-control" id="value" name="value" placeholder="Escribe el valor" required>
                </div>
                <div class="form-group">
                    <button class="btn btn-info" type="submit">Agregar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
