@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <h1 class="text-center"> Editar Rol</h1>
        </div>
        <div class="card-body">
            <form action="{{route('roles.update',$role->id)}}" method="post">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="value">Valor</label>
                    <input type="text" class="form-control" id="value" name="value" placeholder="Escribe el valor" value="{{$role->value}}" required>
                </div>
                <div class="form-group">
                    <button class="btn btn-info" type="submit">Editar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
