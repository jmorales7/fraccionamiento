<?php

namespace App\Http\Controllers;

use App\Schedule;
use App\User;
use Illuminate\Http\Request;

class SchedulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['schedules']=Schedule::all();

        return view('schedules.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['users'] = User::all();

        return view('schedules.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $schedule = new Schedule();
        $schedule->date = $request->date;
        $date = Schedule::where('date',$request->date)->first();
        if(!empty($date->date) && $date->date == $request->date ) {

            return redirect()->back()->with('status', 'Selecciona otra fecha esa ya esta ocupada');
        }
        $schedule->description = $request->description;
        $schedule->status = $request->status;
        $schedule->user_id = $request->user;

        $schedule->save();

        return redirect()->route('schedules.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Schedule $schedule)
    {
        $data['schedule'] = $schedule;
        $data['users'] = User::all();
        return view('schedules.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Schedule $schedule)
    {
        $schedule->date = $request->date;
        $date = Schedule::where('date',$request->date)->first();
        if(!empty($date->date) && $date->date == $request->date ) {

            return redirect()->back()->with('status', 'Selecciona otra fecha esa ya esta ocupada');
        }
        $schedule->description = $request->description;
        $schedule->status = $request->status;
        $schedule->user_id = $request->user;

        $schedule->save();

        return redirect()->route('schedules.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        $schedule->delete();

        return redirect()->route('schedules.index');
    }
}
