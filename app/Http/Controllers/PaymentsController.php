<?php

namespace App\Http\Controllers;

use App\File;
use App\Payment;
use App\User;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['payments'] = Payment::all();
        return view('payments.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['users'] = User::all();
        return view('payments.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payment = new Payment();
        $payment->concept = $request->concept;
        $payment->quantity = $request->quantity;
        $payment->user_id = $request->user;

        $payment->save();

        if ($request->hasFile('image')) {
            $file = new File();
            $image = $request->file('image')->store('public/images');
            $file->file_url = $image;
            $file->payment_id = $payment->id;
            $file->save();
        }

        return redirect()->route('payments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        $data['payment'] = $payment;
        $data['users'] = User::all();
        $data['files'] = File::where('payment_id',$payment->id)->get();
        return view('payments.show',$data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        $data['payment'] = $payment;
        $data['users'] = User::all();

        return view('payments.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        $payment->concept = $request->concept;
        $payment->quantity = $request->quantity;
        $payment->user_id = $request->user;

        $payment->save();

        return redirect()->route('payments.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        $payment->delete();

        return redirect()->route('payments.index');
    }
}
