<?php

namespace App\Http\Controllers;

use App\MaintenancePayment;
use App\User;
use Illuminate\Http\Request;

class MaintenancePaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['payments'] = MaintenancePayment::all();

        return view('maintenance_payments.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['users'] = User::all();

        return view('maintenance_payments.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payment = new MaintenancePayment();
        $payment->concept = $request->concept;
        $payment->quantity = $request->quantity;
        $payment->status = $request->status;
        $payment->user_id = $request->user;
        $payment->save();

        return redirect()->route('maintenance-payments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MaintenancePayment $maintenance_payment)
    {
        $data['payment'] = $maintenance_payment;

        return view('maintenance_payments.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MaintenancePayment $maintenance_payment)
    {
        $maintenance_payment->concept = $request->concept;
        $maintenance_payment->quantity = $request->quantity;
        $maintenance_payment->status = $request->status;
        $maintenance_payment->save();

        return redirect()->route('maintenance-payments.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MaintenancePayment $maintenance_payment)
    {
        $maintenance_payment->delete();

        return redirect()->route('maintenance-payments.index');
    }
}
