<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email'=> 'admin@gmail.com.mx',
            'password' => bcrypt('123456789'),
            'role_id' => 1
        ]);

    }
}
